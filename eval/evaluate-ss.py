#! /usr/bin/env python3

import argparse
import collections
import itertools
import os
import sys
import tsvlib
import pdb


parser = argparse.ArgumentParser(description="""
        Evaluate input `prediction` against `gold` for supersenses.""")
parser.add_argument("--debug", action="store_true",
        help="""Print extra debugging information (you can grep it,
        and should probably pipe it into `less -SR`)""")
parser.add_argument("--gold", metavar="gold_file", dest="gold_file",
        required=True, type=argparse.FileType('r'),
        help="""The reference gold-standard file in .cupt format""")
parser.add_argument("--pred", metavar="prediction_file", 
        dest="prediction_file", required=True, 
        type=argparse.FileType('r'),
        help="""The system prediction file in .cupt format""")

UNLABELED = '<unlabeled>'
TRAIN_FIELD_NAMES = ['FORM', 'LEMMA']

def error(message, **kwargs):
    r"""Print error message and quit."""
    tsvlib.warn(message, warntype="ERROR", **kwargs)
    sys.exit(1)

GOLDPRED_FMT = {
    # gold, pred
    (False, False): "{}",  # normal
    (False,  True): "\x1b[48;5;183m{}\x1b[m",  # pred-only
    (True,  False): "\x1b[48;5;214m{}\x1b[m",  # gold-only
    (True,   True): "\x1b[1;38;5;231;48;5;28m{}\x1b[m", # both (matched!)
}


class Main(object):
    def __init__(self, args):
        self.total_gold = 0  # recall = correct/total_gold
        self.total_pred = 0  # precision = correct/total_pred
        self.correct = 0
        self.name = "Global"    
        sys.excepthook = tsvlib.excepthook
        self.args = args
        # Gold = test.cupt; Pred = test.system.cupt
        if "test.cupt" in self.args.prediction_file.name or "system" in self.args.gold_file.name:
            tsvlib.warn("Something looks wrong in the gold & system arguments.\n" \
                    "Is `{gold_file.name}` really the gold test.cupt file?\n" \
                    "Is `{pred_file.name}` really a system prediction file?",
                    gold_file=self.args.gold_file, pred_file=self.args.prediction_file)


    def increment(self, plus_g, plus_p, plus_correct):
        r'''(Low-level helper method to increment counters for precision and recall).'''
        self.total_gold += plus_g
        self.total_pred += plus_p
        self.correct += plus_correct

    def print_p_r_f(self, prefix: str):
        r'''Prints P/R/F'''
        precision = self.correct / (self.total_pred or 1)
        recall = self.correct / (self.total_gold or 1)
        f1 = 2*precision*recall/(precision+recall) if precision else 0
        print("* {prefix}{self.name}: " \
              "P={self.correct}/{self.total_pred}={precision:.4f} " \
              "R={self.correct}/{self.total_gold}={recall:.4f} " \
              "F={f1:.4f}".format(self=self, precision=precision,
                                    recall=recall, f1=f1, prefix=prefix))


    def run(self):
        if self.args.debug:
            print("DEBUG:  LEGEND:  {} {} {} {}".format(
                GOLDPRED_FMT[(False, False)].format('normal-text'),
                GOLDPRED_FMT[(True, False)].format('gold-only'),
                GOLDPRED_FMT[(False, True)].format('pred-only'),
                GOLDPRED_FMT[(True, True)].format('gold-pred-matched')))
            print("DEBUG:")

        mc_args = dict(debug=self.args.debug, tractable=True)
        self.gold = collections.deque(tsvlib.iter_tsv_sentences(self.args.gold_file))
        self.pred = collections.deque(tsvlib.iter_tsv_sentences(self.args.prediction_file))
        #seen = SeenInfo(self.args.train_file)
        
        while self.gold or self.pred:
            self.check_eof()
            sent_gold = self.gold.popleft()
            sent_pred = self.pred.popleft()
                        #if self.args.debug:
            #    self.print_debug_pairing(sent_gold, sent_pred)
            self.compare_sentences(sent_gold, sent_pred)
            for (w_g,w_p) in zip(sent_gold.words, sent_pred.words):
                g_ss = w_g.get('MISC',None)
                p_ss = w_p.get('MISC',None)
                self.increment(1 if g_ss else 0, 
                               1 if p_ss else 0,  
                               1 if (p_ss and p_ss == g_ss) else 0 )
        

            
        #------------------------------------------
        print("## Global supersense evaluation")
        self.print_p_r_f("Supersense")        

    #=============================================================

    def check_eof(self):
        r"""Generate an error if one of the files is in EOF and the other is not."""
        if not self.gold:
            error("Prediction file is larger than the gold file (extra data?)")
        if not self.pred:
            error("Prediction file is smaller than the gold file (missing data?)")

    def compare_sentences(self, sent_g, sent_p):
        r'''Warn if sentence sizes do not match.'''
        if len(sent_g.words) != len(sent_p.words):
            tsvlib.global_last_lineno(None, 0)
            len_g, len_p = len(sent_g.words), len(sent_p.words)
            tsvlib.warn(
                "Sentence sizes do not match\n" \
                "In sentence starting at `{args.gold_file.name}` "\
                "line {g.lineno_beg} ({len_g} tokens)\n" \
                "In sentence starting at `{args.prediction_file.name}` "\
                "line {p.lineno_beg} ({len_p} tokens)",
                g=sent_g, p=sent_p, args=self.args, len_g=len_g, len_p=len_p)
            return False
        return True

#####################################################

if __name__ == "__main__":
    Main(parser.parse_args()).run()
