#!/usr/bin/python3

"""
Transforms Dimsum's TSV using BIO into CUPT.
"""

import argparse
import pdb
import sys
import re
import os.path

parser = argparse.ArgumentParser(
        description='Transforms Dimsum\'s TSV using BIO into CUPT.')
parser.add_argument('--dimsum-file', type=argparse.FileType("r"), required=True,
                    help='input in dimsum TSV format')
parser.add_argument('--remove-nested', action='store_true',
                    help='Remove tags corresponding to nested MWEs (b,i)')
parser.add_argument('--sent-id-prefix', type=str, default=None,
                    help='Regexp corresponding to sentence ID prefixes to keep')

################################################################################

def print_cupt_sentence(tokens_list, remove_nested=False, sent_id_prefix=None):  
  global sent_counter
  global corpus_uri
  global file_uri
  id_nb = 1
  mwe_nb = 0
  incr = 1
  embedded = False
  open_MWE = None
  sent_id = tokens_list[0][8]
  if sent_id_prefix and not sent_id_prefix.match(sent_id):
    return # Ignore sentence    
  print("# source_sent_id = {} {} {}".format(corpus_uri, file_uri, str(sent_id)))
  print("# sent_counter = {}".format(str(sent_counter)))
  print("# text = " + " ".join(token for token in map(lambda x:x[1],tokens_list)))
  for token in tokens_list:
    mwe_bio = token[4]
    if remove_nested and (mwe_bio[0] in ["b","i"]) : # remove nested MWEs
      mwe_bio = "o"
    # Transform BIO Dimsum/Streusle style tags into CUPT format
    if mwe_bio == "O" or mwe_bio == "o":
      mwe_tag = "*"
      if open_MWE and mwe_bio == "O": #Close open MWE
        open_MWE = None
    elif mwe_bio.startswith("B"):
      mwe_nb += incr # New MWE starts, increment ID number
      incr = 1 # In case there was any embedded MWE
      mwe_tag = str(mwe_nb) + ":" + (mwe_bio[2:] or "X")
      open_MWE = mwe_nb
    elif mwe_bio.startswith("I"):
      if open_MWE :
        mwe_tag = str(open_MWE)
      else:
        mwe_tag = str(mwe_nb)
        print("WARNING: no open MWE! Sent: {}, Tok: {}".format(sent_id,id_nb),
              file=sys.stderr)        
    elif mwe_bio.startswith("b"):
      #embedded = True
      embedded_mwe = mwe_nb + 1
      if open_MWE : incr = 2 # increment for outer and inner MWE next time
      mwe_tag = str(embedded_mwe) + ":" + (mwe_bio[2:] or "X")
    elif mwe_bio.startswith("i"):
      mwe_tag = str(embedded_mwe) 
    else:
      pdb.set_trace()
    if token[3] == "CONJ" : token[3] = "CCONJ" # Update to UD v2 tags
    out_line = token[0:4] + (["_"] * 5) + ["ss="+token[7] if token[7] else "_", mwe_tag]
    id_nb += 1
    print("\t".join(out_line))
  print()
  #if embedded:
  #  pdb.set_trace()
  sent_counter += 1

################################################################################
                    
args = parser.parse_args()

sent_counter = 1
corpus_uri = "https://github.com/dimsum16/dimsum-data"
file_uri = os.path.basename(args.dimsum_file.name)
#head print(file_uri)

fields = ["ID","FORM","LEMMA","UPOS","XPOS","FEATS","HEAD","DEPREL",
          "DEPS","MISC","PARSEME:MWE"]
print("# global.columns = " + " ".join(fields))
if args.sent_id_prefix :
  sent_id_prefix = re.compile(args.sent_id_prefix)
else :
  sent_id_prefix = None
with args.dimsum_file as dimsum_file :
  tokens_list = []
  for line in dimsum_file :
    if not line.strip(): # empty line            
      print_cupt_sentence(tokens_list, args.remove_nested, sent_id_prefix)
      tokens_list = []
    else:
      token_field = line.strip().split("\t")
      tokens_list.append(token_field)
